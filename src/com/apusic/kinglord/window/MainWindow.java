package com.apusic.kinglord.window;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Hashtable;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;
import javax.jcr.Value;
import javax.jcr.nodetype.NodeType;
import javax.jcr.version.VersionHistory;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.apache.jackrabbit.core.config.RepositoryConfig;
import org.apache.jackrabbit.jcr2spi.RepositoryImpl;
import org.apache.jackrabbit.jcr2spi.config.CacheBehaviour;
import org.apache.jackrabbit.spi.IdFactory;
import org.apache.jackrabbit.spi.NameFactory;
import org.apache.jackrabbit.spi.PathFactory;
import org.apache.jackrabbit.spi.QValueFactory;
import org.apache.jackrabbit.spi.RepositoryService;
import org.apache.jackrabbit.spi.commons.identifier.IdFactoryImpl;
import org.apache.jackrabbit.spi.commons.name.NameFactoryImpl;
import org.apache.jackrabbit.spi.commons.name.PathFactoryImpl;
import org.apache.jackrabbit.spi.commons.value.QValueFactoryImpl;
import org.apache.jackrabbit.spi2dav.RepositoryServiceImpl;

public class MainWindow extends JFrame {

    private static final long serialVersionUID    = -7416453778953808812L;
    JButton                   con;
    JButton                   b;
    JButton                   d;
    JTextField                repositoryXmlPath;
    JTextField                jcrPath;
    JTextField                nodePath;
    JTextField                nodeId;
    JTextArea                 result;
    JLabel                    node;
    JLabel                    node1;
    Font                      font                = new Font("", Font.PLAIN, 16);
    Session                   session             = null;
    String                    repository_xml_path = "D:/24PortalWorkspace/km/WebContent/jackrabbit/repository.xml";
    String                    jcr_path            = "D:/24PortalWorkspace/km/WebContent/jackrabbit";
    Repository                repository          = null;

    public Repository getRep() throws Exception {
        if (repository != null) {
            return repository;
        }
        try {
            Map<String, Object> settings = new Hashtable<String, Object>();
            settings.put(Context.INITIAL_CONTEXT_FACTORY,
                         "org.apache.jackrabbit.core.jndi.provider.DummyInitialContextFactory");
            settings.put(Context.PROVIDER_URL, "http://www.apache.org/jackrabbit");
            InitialContext ctx = new InitialContext(new Hashtable<String, Object>(settings));

            try {
                repository = org.apache.jackrabbit.core.RepositoryImpl.create(RepositoryConfig.create(repository_xml_path,
                                                                                                      jcr_path));
            } catch (Exception e1) {
                throw e1;
            }

            if (repository == null) {
                try {
                    repository = (Repository) ctx.lookup("jackrabbit.repository");
                    System.out.println("get repository from JNDI:jackrabbit.repository");
                } catch (Exception e) {
                    throw e;
                }
            }

            if (repository == null) {
                try {
                    System.out.println("get repository from RMI:http://localhost:8080/rmi.");
                } catch (Exception e) {
                    throw e;
                }

            }
            if (repository == null) {
                try {
                    IdFactory idFactory = IdFactoryImpl.getInstance();
                    NameFactory nFactory = NameFactoryImpl.getInstance();
                    PathFactory pFactory = PathFactoryImpl.getInstance();
                    QValueFactory vFactory = QValueFactoryImpl.getInstance();
                    final RepositoryServiceImpl webdavRepoService = new RepositoryServiceImpl(
                                                                                              "http://localhost:8080/server",
                                                                                              idFactory, nFactory,
                                                                                              pFactory, vFactory);

                    org.apache.jackrabbit.jcr2spi.config.RepositoryConfig config = new org.apache.jackrabbit.jcr2spi.config.RepositoryConfig() {

                        public RepositoryService getRepositoryService() {
                            return webdavRepoService;
                        }

                        public String getDefaultWorkspaceName() {
                            return "default";
                        }

                        public CacheBehaviour getCacheBehaviour() {
                            return CacheBehaviour.INVALIDATE;
                        }

                        public int getItemCacheSize() {
                            return 1000;
                        }

                        public int getPollTimeout() {
                            return 20;
                        }
                    };

                    repository = RepositoryImpl.create(config);
                    System.out.println("get repository from DavEx:" + "http://localhost:8080/server");
                } catch (Exception e) {
                    throw e;
                }
            }
            return repository;
        } catch (NamingException e) {
            throw e;
        }
    }

    public MainWindow(){
        this.setTitle("仓库查看器   by kinglord");
        this.setLayout(new FlowLayout(FlowLayout.LEFT));
        this.setBounds(200, 100, 540, 550);
        this.setVisible(true);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        JLabel repositoryXmlPathL = new JLabel("rep xml：");
        this.add(repositoryXmlPathL);

        repositoryXmlPath = new JTextField(40);
        // repositoryXmlPath.setText("J:/Work/24PortalWorkspace/wcm/WebContent/WEB-INF/repository.xml");
        // repositoryXmlPath.setText("F:/aps/domains/mydomain/baseApps/wcm/WEB-INF/default-repository.xml");
        repository_xml_path = this.getClass().getResource("/").getPath() + "/repository.xml";
        repositoryXmlPath.setText(repository_xml_path);
        this.add(repositoryXmlPath);

        JLabel jcrPathL = new JLabel("jcr path：");
        this.add(jcrPathL);

        jcrPath = new JTextField(40);
        this.add(jcrPath);
        // jcrPath.setText("J:/Work/24PortalWorkspace/apache-tomcat-7.0.39/jcr");
        jcrPath.setText("F:/aps/domains/mydomain/wcm/repository");

        jcrPath.setFont(font);
        repositoryXmlPath.setFont(font);

        repositoryXmlPath.addKeyListener(new KeyListener() {

            public void keyTyped(KeyEvent e) {

            }

            public void keyReleased(KeyEvent e) {
                repository = null;
            }

            public void keyPressed(KeyEvent e) {

            }
        });

        jcrPath.addKeyListener(new KeyListener() {

            public void keyTyped(KeyEvent e) {

            }

            public void keyReleased(KeyEvent e) {
                repository = null;
            }

            public void keyPressed(KeyEvent e) {

            }
        });

        node = new JLabel("节点全路径：");
        node.setVisible(false);
        node1 = new JLabel("节点ID：");
        node1.setVisible(false);

        con = new JButton("连接仓库");
        con.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent arg0) {
                repository_xml_path = repositoryXmlPath.getText();
                jcr_path = jcrPath.getText();
                Repository rep;
                try {
                    rep = getRep();
                    if (rep != null) {
                        con.setVisible(false);
                        node.setVisible(true);
                        nodePath.setVisible(true);
                        node1.setVisible(true);
                        nodeId.setVisible(true);
                        b.setVisible(true);
                        d.setVisible(true);
                    } else {
                        result.setText("连接失败");
                    }
                } catch (Exception e) {
                    result.setText(e.getMessage());
                }
            }
        });
        this.add(con);
        this.add(node);
        nodePath = new JTextField(40);
        this.add(nodePath);
        nodePath.setVisible(false);
        this.add(node1);
        nodeId = new JTextField(40);
        this.add(nodeId);
        nodeId.setVisible(false);
        b = new JButton("获取节点数据");
        b.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent arg0) {
                System.out.println(nodePath.getText());
                try {
                    if (session == null || !session.isLive()) {
                        SimpleCredentials sc = new SimpleCredentials("admin", "admin".toCharArray());
                        session = getRep().login(sc);
                        if (session == null || !session.isLive()) {
                            JOptionPane.showMessageDialog(b, "仓库未连接或连接失败！");
                        }
                    } else {
                        try {
                            String nodePathString = nodePath.getText();
                            String nodeIdString = nodeId.getText();
                            if (nodeIdString != null && !nodeIdString.equals("")) {
                                Node jcrNode = session.getNodeByIdentifier(nodeIdString.trim());
                                String info = getInfo(jcrNode);
                                result.setText(info);
                            } else if (nodePathString != null) {
                                Node jcrNode = session.getNode(nodePathString.trim());
                                String info = getInfo(jcrNode);
                                result.setText(info);
                            }
                        } catch (Exception e) {
                            session = null;
                            e.printStackTrace();
                        }
                    }
                } catch (RepositoryException e) {
                    JOptionPane.showMessageDialog(b, e.getMessage());
                    e.printStackTrace();
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(b, e.getMessage());
                    e.printStackTrace();
                }
            }
        });
        this.add(b);
        b.setVisible(false);

        d = new JButton("删除节点");
        d.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent arg0) {
                System.out.println(nodePath.getText());
                try {
                    if (session == null || !session.isLive()) {
                        SimpleCredentials sc = new SimpleCredentials("admin", "admin".toCharArray());
                        session = getRep().login(sc);
                        if (session == null || !session.isLive()) {
                            JOptionPane.showMessageDialog(b, "仓库未连接或连接失败！");
                        }
                    } else {
                        try {
                            String nodePathString = nodePath.getText();
                            String nodeIdString = nodeId.getText();
                            if (nodeIdString != null && !nodeIdString.equals("")) {
                                Node jcrNode = session.getNodeByIdentifier(nodeIdString.trim());
                                String info = getInfo(jcrNode);
                                result.setText(info);
                                jcrNode.remove();
                                session.save();
                            } else if (nodePathString != null) {
                                Node jcrNode = session.getNode(nodePathString.trim());
                                String info = getInfo(jcrNode);
                                result.setText(info);
                                jcrNode.remove();
                                session.save();
                            }
                        } catch (Exception e) {
                            session = null;
                            e.printStackTrace();
                        }
                    }
                } catch (RepositoryException e) {
                    JOptionPane.showMessageDialog(b, e.getMessage());
                    e.printStackTrace();
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(b, e.getMessage());
                    e.printStackTrace();
                }
            }
        });
        this.add(d);
        d.setVisible(false);

        result = new JTextArea();
        result.setRows(13);
        result.setColumns(39);
        result.setFont(font);
        result.setLineWrap(true);
        result.setBackground(Color.black);
        result.setForeground(Color.WHITE);
        result.setAutoscrolls(true);
        result.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
        JScrollPane scroll = new JScrollPane(result);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        this.add(scroll);

        nodePath.setFont(font);
        nodeId.setFont(font);
        this.setResizable(false);
        this.setVisible(true);
    }

    public Session getSession() {
        return this.session;
    }

    private String getInfo(Node node) {
        try {
            String name = node.getName();
            String identifier = node.getIdentifier();
            NodeType primaryNodeType = node.getPrimaryNodeType();
            NodeType[] mixinNodeTypes = node.getMixinNodeTypes();
            NodeIterator nodes = node.getNodes();
            PropertyIterator properties = node.getProperties();
            VersionHistory versionHistory = null;
            try {
                versionHistory = node.getVersionHistory();
            } catch (Exception e) {

            }

            StringBuilder sb = new StringBuilder();
            sb.append("path   :   " + node.getPath());
            sb.append("\n");
            sb.append("name   :   " + name);
            sb.append("\n");
            sb.append("id   :   " + identifier);
            sb.append("\n");
            sb.append("primaryNodeType   :   " + primaryNodeType.getName());
            sb.append("\n");
            sb.append("mixinNodeTypes   :   \n");
            sb.append("     ");
            for (NodeType nt : mixinNodeTypes) {
                sb.append(nt.getName() + " ");
            }
            sb.append("\n");
            sb.append("childNodes   :   \n");
            while (nodes.hasNext()) {
                sb.append("     ");
                Node cn = nodes.nextNode();
                sb.append(cn.getPath() + "\n");
            }
            sb.append("\n");
            sb.append("properties   :   \n");
            while (properties.hasNext()) {
                sb.append("     ");
                Property p = properties.nextProperty();
                if (p.isMultiple()) {
                    sb.append(p.getPath() + "   :   " + p.getValues()[0].getType() + "   :   ");
                    Value[] values = p.getValues();
                    sb.append("(");
                    for (Value v : values) {
                        sb.append("|" + v.getString() + "|");
                    }
                    sb.append(")\n");
                } else {
                    sb.append(p.getPath() + "   :   " + p.getValue().getType() + "   :   " + p.getValue().getString()
                              + "\n");
                }
            }
            sb.append("\n");
            if (versionHistory != null) {
                String[] versionLabels = versionHistory.getVersionLabels();
                sb.append("versionHistory   :   \n");
                sb.append("     ");
                for (String vl : versionLabels) {
                    sb.append(vl + " ");
                }
            }
            return sb.toString();
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @Override
    public void dispose() {
        try {
            if (session != null) {
                session.logout();
            }
        } finally {
            super.dispose();
        }
    }

    public static void main(String[] args) {
        new MainWindow();
    }

}
